/*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.regex.Pattern;

/**
 * - *
 * + *@Param Takes in strings + *@return A string that has no Lowercase letters
 * non letter characters and no spaces
 *
 * @author Michael
 */
public class Helpful {

    public static String Restrict(String construct) {
        construct = construct.toLowerCase();
        //System.out.println("Here I am");
        String getRid = "[^a-z]";
        construct = construct.replaceAll(getRid, "");
        construct = construct.trim();
        return construct;
    }

    /**
     * + *
     * + * @param oFile take in a stack and implements its sort and counts the
     * words. Also a print writer and a bufferedWriter + * @return A new file
     * and a list of stacks that is printed too it +
     */
    public static String Organise(Stack<String> oFile) {
        HashMap<String, Integer> map = new HashMap<String, Integer>();

        while (!oFile.isEmpty()) {
            String contain = oFile.pop();
            //System.out.println(contain);
            if (map.containsKey(contain)) {
                int current = map.get(contain) + 1;
                map.put(contain, current);

            } else {
                map.put(contain, 1);
            }
        }
        File theReturn = new File("bigger.txt");
        PrintWriter Print = null;
        BufferedWriter out = null;
        try {
            Print = new PrintWriter("bigger.txt");
        } catch (FileNotFoundException e) {

        }
        try {
            out = new BufferedWriter(Print);

            out.append(Sort(map));
        } catch (IOException e) {
            System.out.println("Something went wrong");
        }
        try {
            out.close();
        } catch (IOException e) {
            System.out.print("We won't let you leave");
        }
        return "";
    }

    /**
     * + *
     * + * @param Store values in a Hashmap and have values and keys put into a
     * stack and have them sorted + * @return Two sorted arrays for the keys and
     * values +
     */
    public static String Sort(HashMap<String, Integer> Store) {
        Set<String> kStore = Store.keySet();
        String[] contKeys = new String[kStore.size()];
        int[] kValues = new int[kStore.size()];
        int S = 0;
        for (String answ : kStore) {
            contKeys[S] = answ;
            kValues[S] = Store.get(answ);
            S++;

        }
        String answer = "";
        //Bubble sort for time complexity
        int n = kValues.length;
        boolean moved = false;
        for (int i = 0; i < n - 1; i++) 
        {
            //           System.out.println("Here I am");
            for (int s = i + 1; s < n - i - 1; s++) 
            {
                moved = false;
                if (kValues[s] > kValues[s + 1]) 
                {
                    int temps = kValues[s];
                    kValues[s] = kValues[s + 1];
                    kValues[s + 1] = temps;
                    
                    String temp = contKeys[s];
                    contKeys[s] = contKeys[s + 1];
                    contKeys[s + 1] = temp;
                    moved = true;
                }
            
            }
        }

//            for (int k = 0; k < contKeys.length; k++)//Prints out the Key's and values aka Word's and time's its used
//            {
//                answer += kValues[k];
//                answer += " , ";
//                answer += contKeys[k];
//                answer += "\n";
//            }

            for (int c = 0; c < 6; c++)//Prints out the Key's and values aka Word's and time's its used
            {
                answer += kValues[c];
                answer += " , ";
                answer += contKeys[c];
                answer += "\n";
                
                System.out.println(kValues[c]);
                System.out.print(",");
                System.out.print(contKeys[c]);
                System.out.print("\n");
            }
                answer+= "_______________";
                System.out.println("_______________");
            for (int k = kValues.length-6 ; k < kValues.length - 1; k++)//Prints out the Key's and values aka Word's and time's its used
            {
                answer += kValues[k];
                answer += " , ";
                answer += contKeys[k];
                answer += "\n";
                
                
                System.out.print(kValues[k]);
                System.out.print(",");
                System.out.print(contKeys[k]);
                System.out.print("\n");
                
            }

            
        
        return answer;
    }
}
